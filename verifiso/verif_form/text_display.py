import tkinter as tk

class Text_display(tk.Frame):

    def __init__(self, container, text_width, text_height):
        super().__init__()
        self.master = container
        self.create_widgets(text_width, text_height)

    def create_widgets(self, text_width, text_height):
        self.scroll = tk.Scrollbar(self)
        self.scroll.pack(side='right', fill='y')
        self.text = tk.Text(self,
                                    state=tk.DISABLED,
                                    yscrollcommand = self.scroll.set,
                                    width = text_width,
                                    height= text_height,
                                    )
        self.text.pack(side='right')
        self.scroll.configure(command=self.text.yview)


 
