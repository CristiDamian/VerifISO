import tkinter as tk
from tkinter.filedialog import askopenfilename

class Label_entry_browse(tk.Frame):

    def __init__(self, container, label_text, has_browse=False):
        super().__init__()
        self.master = container
        self.create_widgets(label_text)
        if has_browse:
                self.create_browse()

    def create_widgets(self, label_text):
        self.label = tk.Label(self, text=label_text)
        self.label.pack(side='left')
        self.contents = tk.StringVar()
        self.entry = tk.Entry(self, textvariable=self.contents, bg='white')
        self.entry.pack(side='left',fill='x', expand=True)

    def create_browse(self):
        self.button = tk.Button(self,
                                text="Browse",
                                command=self.on_browse,
                                )
        self.button.pack(side='left')

    def on_browse(self, event=None):
        self.contents.set(askopenfilename())
        self.entry.xview('end')

