import tkinter as tk
from .label_entry_browse import Label_entry_browse
from .text_display import Text_display


class VerifisoApp(tk.Frame):
    def __init__(self, container, method_available):
        super().__init__()
        self.method_available = method_available
        self.signed_options = dict()
        container.title("VerifISO")
        self.master = container
        self.create_widgets()
        self.pack()

    def create_widgets(self):
        self.horiz = tk.Frame(self)
        self.target = Label_entry_browse(self,
                                         label_text='Target file:',
                                         has_browse=True,
                                         )
        self.target_path = self.target.contents
        self.digest = Label_entry_browse(self,
                                         label_text='Digest file:',
                                         has_browse=True,
                                         )
        self.digest_path = self.digest.contents
        self.method_label = tk.Label(self.digest, text="type:")
        self.method = tk.StringVar(self)
        self.method.set('auto')
        self.method_menu = tk.OptionMenu(self.digest, self.method, *self.method_available)

        # Method and check button
        self.check_button = tk.Button(self.horiz, text="Check")
        # Check result
        self.check_text = Text_display(self, 44, 3)
        self.check_result = self.check_text.text
        self.check_result.tag_config('red', foreground="red")
        self.check_result.tag_config('green', foreground="green")
        # Authenticate side
        self.signature = Label_entry_browse(self,
                                            label_text='Signature file:',
                                            has_browse=True,
                                            )
        self.signature_path = self.signature.contents
        self.signed_label = tk.Label(self.signature, text="for")
        self.signed = tk.StringVar(self)
        self.signed.set('Target')
        self.signed_options['Target'] = self.target_path
        self.signed_options['Digest'] = self.digest_path
        self.signed_menu = tk.OptionMenu(self.signature, self.signed, *self.signed_options.keys())

        self.keyserv = Label_entry_browse(self,
                                          label_text='Key server:',
                                          has_browse=False,
                                          )
        self.auth_button = tk.Button(self.horiz, text='Authenticate')



        self.target.pack(side='top',fill='x',expand=True)
        self.method_label.pack(side='left')
        self.method_menu.pack(side='left')
        self.digest.pack(side='top',fill='x',expand=True)
        self.signed_label.pack(side='left')
        self.signed_menu.pack(side='left')
        self.signature.pack(side='top',fill='x',expand=True)
        self.keyserv.pack(side='top',fill='x',expand=True)
        self.check_button.pack(side='left')
        self.auth_button.pack(side='left')
        self.horiz.pack(side='top')
        self.check_text.pack(side='bottom')








