#!/usr/bin/env python3
"""\
Checks the integrity and authenticity (todo) of a file using checksums and GPG.
"""
import argparse

from .verif_utils import *


parser = argparse.ArgumentParser(description="Checks the integrity and authenticity (todo) of a file using checksums and GPG.")
parser.add_argument('test', help="The file to be checked")
parser.add_argument('--with', dest='method', help="Hashing algorithm used",
                    choices=hash_algos)
parser.add_argument('--in', dest='digest', required=True, help="The file containing checksums")
parser.add_argument('--signed', help="The gpg signature")
parser.add_argument('--keyserv', help="The server that holds the public key.")


def main(args):

    method = (args.method
              if args.method is not None
              else get_auto_method(args.digest))

    try:
        file_hash = get_file_hash(args.test, method)
        print('Hash')
        print(file_hash)

        match_line, matched_name = check_hash(args.digest, file_hash, args.test)
    except TargetNotFoundError as e:
        print(e)
        return
    except ChecksumNotFoundError as e:
        print(e)
        return

    if match_line is None :
        print('File does not match any checksum in the list.')
        return

    if matched_name:
        print('Filename and hash matched the line below:')
    else:
        print('Hash matched the line below:')
    print(match_line)


    if args.signed is None:
        print('You should also verify the authenticity of the file.')
        print('Download the signature from the site and check it')
        print('by adding its path to the `--signed` option.')
        return

    try:
        signee_fingerprint, signee_uid = verify_sign_file(args.digest, args.signed, args.keyserv)
        print("Signature OK")
        print("Fingerprint:", signee_fingerprint)
        print("Signee is:", signee_uid)
        print("Please check if the credentials are correct.")

    except SigningException as e:
        print(e)


if __name__ == '__main__':
    main(parser.parse_args())
