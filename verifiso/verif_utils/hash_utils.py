import hashlib
from hashlib import algorithms_guaranteed
from os import path

hash_algos = algorithms_guaranteed
CHUNK_LEN = 4096

def get_auto_method(digest_path):
    "Returns a hash method based on the filename of the digest file."
    digest_name = path.basename(digest_path).lower()
    methods = (x for x in hash_algos if x in digest_name)
    return next(methods,'sha512')

class TargetNotFoundError(Exception):
    "Target file was not found."
    pass

def get_file_hash(file_path, method):
    "Returns the hexdigest of a file."
    try:
        a_hasher = hashlib.new(method);
        with open(file_path,'rb') as the_file:
            while True:
                the_bytes = the_file.read(CHUNK_LEN)
                a_hasher.update(the_bytes)
                if len(the_bytes) < CHUNK_LEN:
                    return a_hasher.hexdigest()
    except FileNotFoundError as e:
        raise TargetNotFoundError(f"Could not find target file: {str(e)}")

class ChecksumNotFoundError(Exception):
    "Digest file is not found."
    pass

def check_hash(checksum_path, file_hash, file_name):
    """\
        Finds if the file digest is in the digest file.

        Parameters
        ----------
        checksum_path (str):
            The path to de digest file used to verify the file.

        file_hash (str):
            Hex digest of the file to be verified.

        file_name (str):
            The base name of the file to be verfied.

        Returns
        --------
        matched_line (str):
            the line with the file hash, it's none if the hash was not found.

        name_matched (bool):
            If the file name is on the line with the file hash.
    """
    try:
        with open(checksum_path,'r') as the_file:
            for a_line in the_file:
                if file_hash in a_line.split():
                    matched_line = a_line.replace(file_hash,'')
                    name_matched = path.basename(file_name) in matched_line
                    return a_line, name_matched
        return None, False
    except FileNotFoundError as e:
        raise ChecksumNotFoundError(f"Could not find checksum file: {str(e)}")
