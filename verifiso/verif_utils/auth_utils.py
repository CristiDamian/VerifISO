import gpg_lite
from gpg_lite.cmd import GPGError
from gpg_lite.keyserver import KeyserverError, KeyserverKeyNotFoundError

class GPG_custom(gpg_lite.GPGStore):
    """\
    Provides a method for getting the fingerprint of the signature and
    to verify a file with the signature.
    """
    def get_fingerprint(self, signature):
        "Returns the fingerprint of a signature."
        randbytes =b'xxxx'
        with open(signature, mode="rb") as f_signature:
            try:
                return self.verify_detached_sig(src=randbytes, sig=f_signature.read())
            except GPGError as err:
                errmsg = str(err).split()
                for words in zip(errmsg, errmsg[1:], errmsg[2:]):
                    if words[0] == 'RSA' and words[1] == 'key':
                                return words[2]

    def file_verify_sig(self, test, signature):
        """If the signature is ok it returns the fingerprint of the signature
        otherwise it throws an exception."""
        with open(test, mode="rb") as f:
            with open(signature, mode="rb") as f_signature:
                    return self.verify_detached_sig(src=f, sig=f_signature.read())


class SigningException(Exception):
    """\
    Exception thrown while verifying the signature.
    """
    pass

def verify_sign_file(signed_path, sign_path, keyserv):
    """\
    Verifies the authenticity of a file using a detached PGP signature and
    a public key server.

    Parameters
    -----------
    signed_path (str):
        Path to the file to be verified.

    sign_path (str):
        Path to the signature file.

    keyserv (str):
        Domain name of the public key server used.

    Returns
    -------
    signee_fingerprint (str):
        Fingerprint of signature.

    signee_uid (str):
        The user id of the signee of the file.

    Execeptions
    -----------
    SigningException:
        An exception describing the error.

    """
    try:
        store = GPG_custom()
        signee_fingerprint = store.get_fingerprint(sign_path)
        signees = store.list_pub_keys([signee_fingerprint])
        if len(signees)==0 and keyserv is not None and keyserv != '':
            store.recv_keys(signee_fingerprint, keyserver=keyserv)
            signees = store.list_pub_keys([signee_fingerprint])

        signee_fingerprint = store.file_verify_sig(signed_path, sign_path)
        signee_uid = str(signees[0].uids[0])
        return signee_fingerprint, signee_uid

    except ValueError as e:
        if "Invalid URL" in str(e):
            raise SigningException("Invalid key server URL")

    except FileNotFoundError as e:
        raise SigningException(str(e))

    except KeyserverError as e:
        raise SigningException(str(e))

    except KeyserverKeyNotFoundError as e:
        raise SigningException(str(e))

    except GPGError as err:
        errmsg = str(err).split()

        if 'NO_PUBKEY' in errmsg:
            raise SigningException('Public key for signature not found in local keyring. Specify the keyserver indicated on the site to download the key.')

        elif 'BADSIG' in errmsg:
            raise SigningException("Signature is invalid. Make sure you downloaded the right file or signature.")
