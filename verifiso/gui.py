#!/usr/bin/env python3
import tkinter as tk
from os import path
from concurrent.futures.thread import ThreadPoolExecutor

from . import verif_utils as vf
from .verif_form import VerifisoApp


def do_check(the_frame, event=None):
    the_frame.master.configure(cursor='watch')
    target_path = the_frame.target_path.get()
    target_name = path.basename(target_path)
    digest_path = the_frame.digest_path.get()
    message = ''
    method = the_frame.method.get()
    if method == 'auto':
        method = vf.get_auto_method(digest_path)

    the_frame.check_result.configure(state=tk.NORMAL)
    try:
        target_hash = vf.get_file_hash(target_path, method)
        matched_line, name_matched = vf.check_hash(digest_path, target_hash, target_name)
        if matched_line is None:
            the_frame.check_result.insert('end',"Target file does not match the digest file.\n")
            the_frame.check_result.insert('end',"Check failed.\n", 'red')
        else:
            the_frame.check_result.insert('end',"Target file matches the digest file.\n")
            if name_matched:
                the_frame.check_result.insert('end',"Target file name matches the digest file.\n")
            the_frame.check_result.insert('end',"Check succeded.\n",'green')

    except vf.TargetNotFoundError as e:
        the_frame.check_result.insert('end',str(e)+'\n')

    except vf.ChecksumNotFoundError as e:
        the_frame.check_result.insert('end',str(e)+'\n')

    finally:
        the_frame.master.configure(cursor='')
        the_frame.check_result.see('end')
        the_frame.check_result.configure(state=tk.DISABLED)

def do_auth(the_frame, event=None):
    the_frame.master.configure(cursor='watch')
    option = the_frame.signed.get()
    signed_path = the_frame.signed_options[option].get()
    sign_path = the_frame.signature_path.get()
    keyserv = the_frame.keyserv.contents.get()
    the_frame.check_result.configure(state=tk.NORMAL)
    try:
        signee_fingerprint, signee_uid = vf.verify_sign_file(signed_path, sign_path, keyserv)
        the_frame.check_result.insert('end', "Signature OK\n", "green")
        the_frame.check_result.insert('end', f"Fingerprint: {signee_fingerprint}\n")
        the_frame.check_result.insert('end', f"Signee is: {signee_uid}\n")
        the_frame.check_result.insert('end',"Please check if the credentials are correct.")

    except vf.SigningException as e:
        the_frame.check_result.insert('end',str(e)+'\n')

    finally:
        the_frame.master.configure(cursor='')
        the_frame.check_result.see('end')
        the_frame.check_result.configure(state=tk.DISABLED)


def main():
    root = tk.Tk()
    myapp = VerifisoApp(root, ['auto'] + list(vf.hash_algos))
    myapp.pack()
    with ThreadPoolExecutor(max_workers=1) as exe:
        myapp.check_button.configure(command=lambda: exe.submit(do_check, myapp))
        myapp.auth_button.configure(command=lambda: exe.submit(do_auth, myapp))
        myapp.mainloop()

if __name__ == '__main__':
    main()
