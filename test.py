import unittest
from os import path
import verifiso.verif_utils as vf

KEYSERV = 'keyserver.ubuntu.com'
TEST_DATA = 'test_data'

hash_test_params = [('debian-11.4.0-amd64-netinst.iso', 'sha512', 'eeab770236777e588f6ce0f984a7f3e85d86295625010e78a0fca3e873f78188af7966b53319dde3ddcaaaa5d6b9c803e4d80470755e75796fbf0e96c973507f'),
                    ('MX-21.1_fluxbox_x64.iso','sha256','593795b3ddb9e87faf58f521e0661bb7b4ed84be82414d3b7b0bf2462237a73b'),
                    ('MX-21.1_fluxbox_x64.iso','md5','d038f653922ec077cb52fd914e8e1a20'),
                     ]

check_hash_params = [
        ('SHA512SUMS', 'eeab770236777e588f6ce0f984a7f3e85d86295625010e78a0fca3e873f78188af7966b53319dde3ddcaaaa5d6b9c803e4d80470755e75796fbf0e96c973507f','debian-11.4.0-amd64-netinst.iso'),
        ('MX-21.1_fluxbox_x64.iso.sha256','593795b3ddb9e87faf58f521e0661bb7b4ed84be82414d3b7b0bf2462237a73b', 'MX-21.1_fluxbox_x64.iso'),
        ('MX-21.1_fluxbox_x64.iso.md5','d038f653922ec077cb52fd914e8e1a20','MX-21.1_fluxbox_x64.iso'),
        ]

get_fingerprint_params = [
    ('SHA512SUMS.sig','DF9B9C49EAA9298432589D76DA87E80D6294BE9B'),
    ('MX-21.1_fluxbox_x64.iso.sig', 'F56C15AA352A5C50A8391BA1E90429470A677B96'),
    ]

verify_sig_params = [
    ('SHA512SUMS', 'SHA512SUMS.sig'),
    ('MX-21.1_fluxbox_x64.iso', 'MX-21.1_fluxbox_x64.iso.sig'),
    ]

class Utils_test(unittest.TestCase):

        def test_get_file_hash(self):
            for file_name, method, expected_hash in  hash_test_params:
                file_path = path.join(TEST_DATA, file_name)
                with self.subTest(file=file_name):
                    self.assertEqual(
                        vf.get_file_hash(file_path,method),
                        expected_hash)

        def test_get_file_hash_missing_file(self):
            file_path = path.join('test_nata','debian-11.4.0-amd64-netinst.iso')
            method = 'sha512'
            with self.assertRaises(vf.TargetNotFoundError):
                vf.get_file_hash(file_path,method)

        def test_check_hash(self):
            for checksums, file_hash, file_name in check_hash_params:
                checksums_path = path.join(TEST_DATA,checksums)
                with self.subTest(file=checksums):
                    match_line, name_matched = vf.check_hash(checksums_path, file_hash, file_name)
                    self.assertIsInstance(match_line, str)
                    self.assertTrue(name_matched)

        def test_check_hash_wrong_name(self):
            for checksums, file_hash, file_name in check_hash_params:
                checksums_path = path.join(TEST_DATA,checksums)
                file_name = f"wrong_{file_name}"
                with self.subTest(file=checksums):
                    match_line, name_matched = vf.check_hash(checksums_path, file_hash, file_name)
                    self.assertIsInstance(match_line, str)
                    self.assertFalse(name_matched)

        def test_check_hash_no_checksum(self):
            checksums_path = path.join(TEST_DATA,'sha111sums.csv')
            file_hash = 'c9ac5607cfc736bd431689043712cc7282c7102de99bcad1ad98e5178b0da3d0'
            file_name = 'minesweeper.rb'
            with self.assertRaises(vf.ChecksumNotFoundError):
                    _ = vf.check_hash(checksums_path, file_hash, file_name)

        def test_verify_sign_file(self):
            for signed_name, sign_name in verify_sig_params:
                signed_path = path.join(TEST_DATA, signed_name)
                sign_path = path.join(TEST_DATA, sign_name)
                finger, user = vf.verify_sign_file(signed_path, sign_path, KEYSERV)
                self.assertIsInstance(finger, str)
                self.assertIsInstance(user, str)

        def test_verify_sign_file_missing_sign(self):
            for signed_name, sign_name in verify_sig_params:
                signed_path = path.join(TEST_DATA, signed_name)
                sign_path = path.join(TEST_DATA,'wrong', sign_name)
                with self.assertRaises(vf.SigningException):
                    finger, user = vf.verify_sign_file(signed_path, sign_path, KEYSERV)

        def test_verify_sign_file_missing_signed(self):
            for signed_name, sign_name in verify_sig_params:
                signed_path = path.join(TEST_DATA, 'wrong',signed_name)
                sign_path = path.join(TEST_DATA, sign_name)
                with self.assertRaises(vf.SigningException):
                    finger, user = vf.verify_sign_file(signed_path, sign_path, KEYSERV)



if __name__ == '__main__':
    unittest.main()
