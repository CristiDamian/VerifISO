from setuptools import setup

with open('requirements.txt','r') as rfile:
    requirements = rfile.read().splitlines()

setup(
    name='verifiso',
    version='0.0.1',
    description="Verify Linux ISO's more easily.",
    url='https://gitlab.com/CristiDamian/VerifISO',
    author='Cristian Damian',
    license='GNU GPLv3',
    packages=['verifiso'],
    install_requires=requirements,
)
